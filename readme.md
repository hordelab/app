# Horde Web

Vue-based web interface for Horde, the AI Agent IDE.

# Installation

- Clone the repository `git clone https://gitlab.com/shiveringocelot/horde-web.git`
- Pull the necessary dependencies with `git submodule init` and then `git submodule update`
- Install npm dependencies with `npm install`
- Run Horde with `bash run.sh` or `npm run dev`

# Tabs

The Horde application has its functionality divided in main tabs

## Home

Here you configure your Horde app and what it needs to run. Horde needs a place on disk to save its information, so you must select a Project when you first start. When it starts, if it does not exist, horde generates a special folder inside the project called .horde where it saves everything, and the rest of the files are manipulable by the agents.

![](screenshots/home.png)

## Chats

After using Horde for a while you will have many chats with many different agents. The chat list allows you to filter messages by name using the search field at the top left, and also allows you to view the chats you have with a specific agent using the field at the top right.

![](screenshots/chats.png)

## Agents

Here you have all your bots. A bot has: a name, a prompt, and a base script. The prompt tells the agent what personality to take when responding in the chat or when doing their job (Some scripts don't use the prompt). The Script is the "brain" of the agent, it is the sequence of steps that an agent follows when responding to each response.

![](screenshots/agents.png)

## Tasks, Scripts, Plugins

TODO. This is the most powerful part of the app but is not well documented.

The "Chat" script is a simple loop that responds to messages directly, but more complex scripts can guide the agent through a complex process. All agents need a base script to function.

![](screenshots/script.png)
![](screenshots/task.png)






# Vue Template Readme

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
