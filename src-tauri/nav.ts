import { computed, reactive } from "vue";

//import { navStore } from "./stores/nav";

let listeners = {
    change: [],
};

const nav = {
    name: '',
    params: {},

    //get reactive () { return reactive(nav); },

    go (name, params={}) {
        let r = reactive(nav);
        r.name = name;
        r.params = params;

        for (let f of listeners.change) {
            f(nav);
        }
    },

    on (event, callback) {
        listeners[event].push(callback);
    }
};

export default reactive(nav);