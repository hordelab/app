import { ref, watch, computed, reactive } from "vue";

import * as horde from "./horde";

const EXTENSION = ".json";


// Class only used for wrapping values when they must be set bypassing sync
function HydrateUpdate (value) {
    this.value = value;
}

function hydrate (base, ext, path, basePath, listeners) {
    function join (base, ext) {
        return base ? `${base}.${ext}` : ext;
    }

    async function sendUpdate (subpath, value) {
        function sanitize (value) {
            if (value instanceof horde.Stream) {
                return {_horde: "stream", id: value.id};

            } else if (value instanceof Array) {
                return value.map(sanitize);

            } else if (value instanceof Object) {
                let tgt = {};
                for (let key in value) {
                    tgt[key] = sanitize(value[key]);
                }
                return tgt;

            } else {
                return value;
            }
        }

        await horde.handle("model/update", {
            path, subpath, value: sanitize(value)
        });
    }

    function listen (subpath, fn) {
        listeners.push({subpath, fn});
    }

    // Will create properties in target replicating src
    function proxify (target, src, parent = '') {
        let keys;

        function hydrateField (key, value) {
            const subpath = join(parent, key);

            let innerVal;
            Object.defineProperty(target, key, {
                enumerable: true,
                configurable: true,
                get: function() {
                    return innerVal;
                },
                set: function(val) {
                    if (val instanceof HydrateUpdate) {
                        val = val.value;

                        // Don't proxify streams
                        if (val instanceof Object && !(val instanceof horde.Stream)) {
                            innerVal = {};
                            proxify(innerVal, val, subpath);
                        } else {
                            innerVal = val;
                        }
                    } else {
                        sendUpdate(subpath, val);
                    }
                }
            });

            function innerSet (val) {
                if (val instanceof Object && val._horde == "stream") {
                    val = new horde.Stream(val.id);
                }

                reactive(target)[key] = new HydrateUpdate(val);
            }

            listen(subpath, innerSet);

            if (value === undefined) { value = src[key]; }
            innerSet(value);
        }

        if (src instanceof Array) {
            for (let i = 0; i < src.length; i++) {
                hydrateField(String(i));
            }

            let addSubpath = join(parent, 'add');

            target.length = src.length;
            target.push = function (val) {
                sendUpdate(addSubpath, val);
            }
            target.map = function (fn) {
                return Array.from(target).map(fn);
            }
            target.find = function (val) {
                return Array.from(target).find(val);
            }

            listen(addSubpath, val => {
                hydrateField(target.length++, val);
            });
        } else {
            for (let key in src) {
                hydrateField(key);
            }
        }
    }

    proxify(base, ext);
}

export class Instance {
    constructor (model, id = null) {
        this.id = id;
        this.model = model;
        this.data = {};
    }

    get (key) {
        // TODO: key can be a nested field path, separated with `.`
        this.data[key];
    }

    setRaw (key, value) {
        if (key == '*') {
            for (let key in value) {
                this.data[key] = value[key];
            }

            for (key in this.data) {
                if (value[key] === undefined) {
                    delete this.data[key];
                }
            }
        } else {
            // TODO: key can be a nested field path, separated with `.`
            this.data[key];
        }

        // TODO: Update proxy
    }

    set (key, value) {
        this.setRaw(key, value);
        // TODO: Horde send update
    }

    async watch (model) {
        if (this._cancelWatch) return;

        let idPath = this.model.idToPath(this.id);

        this._cancelWatch = await horde.observe(idPath, (subpath, value) => {
            this.setRaw(subpath, value);

            /*
            if (subpath == "*") {
                hydrate(thisVal, value, idPath, "", listeners);

                if (thisVal._loadResolve) {
                    thisVal._loadResolve();
                    thisVal._loadResolve = null;
                }
            } else {
                let entry = listeners.find(entry =>
                    entry.subpath == subpath
                );

                if (entry) {
                    entry.fn(value);
                } else {
                    throw new Error("Update without listener: " + subpath);
                }

                //let subvalue = getSubvalue(r.value, subpath);
                //hydrate(listeners, subvalue, value, subpath);
                // TODO: Handle other path changes
            }
            */
        });
    }

    getProxy () {
        if (this._proxy) return this._proxy;

        function getPathProxy (path) {
            const target = {};

            const handler = {
              get(target, prop, receiver) {
                if (prop === "message2") {
                  return "world";
                }
                return Reflect.get(...arguments);
              },
            };

            const proxy2 = new Proxy(target, handler);
        }


        let proxy = new Proxy();

        throw new Error("getProxy Unimplemented")
    }
}

export class Model {
    // == Static Methods == //
    // In these, `this` refers to the Model subclass.

    static listReactive () {
        if (this._entriesReactive == null) {
            this._entriesReactive = ref([]);

            let _cancelPromise = horde.observe(this._path, (path, value) => {
                if (path == "*") {
                    // Value is an array of paths
                    this._entriesReactive.value = Object.entries(value).map(pair => {
                        let id = this.pathToId(pair[0]);
                        return Object.assign(new this(), pair[1], {id});
                    });
                } else {
                    let id = this.pathToId(path);

                    let entries = this._entriesReactive.value;

                    let it = entries.find(entry => entry.id == id);

                    if (it) {
                        console.log("Update", path);
                        delete value.id;
                        Object.assign(it, value);
                    } else {
                        console.log("Update create", path);
                        it = Object.assign(new this(), value, {id});
                        entries.push(it)
                    }
                }
            });
        }

        return this._entriesReactive;
    }

    static async list () {
        let {content} = await horde.handle("files/read", {path: this._path});
        return (content || []).map(dirEntry => {

            let id = this.pathToId(this._path + "/" + dirEntry);
            if (id == null) return null;

            return {
                id,
                // Remove extension
                title: dirEntry.replace(/\.[^/.]+$/, ""),
            }
        }).filter(x=>x);
    }

    // get and create are async because most likely they will come from requests from the Horde API

    static async create () {
        let {path} = await horde.handle("model/create", {path: this._path});

        return await this.get(this.pathToId(path));
    }

    static pathToId (path) {
        let prefix = this._path + "/"
        if (!path.startsWith(prefix)) return null;
        if (!path.endsWith(EXTENSION)) return null;

        return path.slice(prefix.length, -EXTENSION.length);
    }

    static idToPath (id) {
        return this._path + '/' + id + EXTENSION;
    }

    static async get (id) {
        let thisVal = this.getInner(id);
        await thisVal._loadPromise;
        return thisVal;
    }

    static getReactive(getter) {
        return reactive(this.getInner(getter));

        return reactive(this.getInstance(id).getProxy());
    }

    static getInstance (id) {
        return new Instance(this, id);
    }

    static getInner(getter) {
        let thisVal = new this();

        if (this._config.default) {
            Object.assign(thisVal, this._config.default);
        }

        if (typeof getter === "string" || typeof getter === "number") {
            let val = getter;
            getter = () => val;
        }

        thisVal._loadPromise = new Promise(resolve => {
            thisVal._loadResolve = resolve;
        });

        let cancel;
        watch(getter, async (newval, oldval) => {
            let id = newval;
            let idPath = this.idToPath(id);

            if (id && newval != oldval) {
                thisVal.id = id;
                if (cancel) cancel();

                let listeners = [];
                cancel = await horde.observe(idPath, (subpath, value) => {
                    if (subpath == "*") {
                        hydrate(thisVal, value, idPath, "", listeners);

                        if (thisVal._loadResolve) {
                            thisVal._loadResolve();
                            thisVal._loadResolve = null;
                        }
                    } else {
                        let entry = listeners.find(entry =>
                            entry.subpath == subpath
                        );

                        if (entry) {
                            entry.fn(value);
                        } else {
                            throw new Error("Update without listener: " + subpath);
                        }

                        //let subvalue = getSubvalue(r.value, subpath);
                        //hydrate(listeners, subvalue, value, subpath);
                        // TODO: Handle other path changes
                    }
                });
            }
        }, {immediate: true});

        return thisVal;
    }

    static get_with_key_fn<T> (needle: T, key_fn: (arg0: any) => T) {
        throw new Error("Unimplemented");
    }

    static optionsReactive () {
        return computed(() => {
            return Object.fromEntries(
                this.listReactive().value.map(entry => {
                    return [entry.id, entry.title || entry.name || entry.id];
                })
            );
        });
    }

    static delete (id) {
        throw new Error("Unimplemented");
    }

    static async loadAll () {
        throw new Error("Unimplemented");
    }

    static _updateReactive () {
        throw new Error("Unimplemented");
    }

    static configure (params) {
        this._path = params.path;

        this._config = params;

        this.fieldMaps = {};
        if (params.fields) {
            Object.assign(this.fieldMaps, params.fields);
        }

        this._entries = new Map();
        //this._entriesReactive = ref([]);

        //throw new Error("Unimplemented");
    }

    static async saveAll () {
        throw new Error("Unimplemented");
    }
}

export default Model;