//import * as dialog from '@tauri-apps/api/dialog';
//import { emit } from '@tauri-apps/api/event'

export async function confirm (prompt) {
    return window.confirm(prompt);
}

export async function input (prompt) {
    return window.prompt(prompt);
}

export async function inputFile (options) {
    console.log("app.inputFile", options);
    throw new Error("Cannot open files");
    //return await dialog.open(options);
}

export function alert (message) {
    throw new Error("Unimplemented: app.alert");
    //emit("alert", {message});
}


// === Events === //

const listeners = {};

export function emit (name, ...args) {
    const fs = listeners[name];
    if (fs == null) return;

    for (let f of fs) {
        f(...args);
    }
}

export function listen (name, fn) {
    let fs = listeners[name];
    if (fs == null) {
        listeners[name] = fs = [];
    }

    fs.push(fn);
}

export function invoke (name, ...args) {
    throw new Error("invoke");
}