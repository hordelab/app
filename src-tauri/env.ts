// The environment is where the program is running
// and where the agents are located and capable to act

// getProjectPath NO SE DEBE ALMACENAR
// el usuario puede cambiarlo en cualquier momento
import * as config from "./config";
import * as app from "./app";

import * as horde from "./horde";


// File System

// Will read the file if exists, or return null if there's no file in the directory
export async function readFile (filePath: string): Promise<string> {
    let res = await horde.handle("files/read", {path: filePath});
    if (res.file) {
        return res.content;
    } else {
        return null;
    }
}

export async function writeFileAdmin (filePath, content) {
    let res = await horde.handle("files/write", {path: filePath, content});
    /*
    if (res.success) {
        return true;
    } else {
        return false;
    }
    */
    return true;
}

export async function writeFile (filePath, content) {
    app.alert(`Write File: ${filePath}, ${content.length} characters`);
    return await writeFileAdmin(filePath);
}

export async function listDir (dirPath: string) {
    let res = await horde.handle("files/read", {path: dirPath});
    if (res.directory) {
        return res.content;
    } else {
        return null;
    }
}

let cmdCount = 0;
let cmdQueue = {};
let cmdListeners = {};

async function waitCmd (id) {
    if (cmdQueue[id]) {
        let result = cmdQueue[id];
        delete cmdQueue[id];
        return result;
    } else {
        return await new Promise(resolve => {
            cmdListeners[id] = resolve;
        });
    }
}

export async function cmd (cmd, ...args) {
    app.alert(`Execute command: ${cmd} ${args.join(' ')}`);

    let id = cmdCount++;
    let result = await app.invoke("cmd", {
        id: String(id),
        args: [cmd, ...args],
    });

    return await waitCmd(id);
}

const _unlistenCmd = await app.listen("cmd-result", event => {
    console.log('cmd-result', event.payload);
    let {id, exit_code, stdout} = event.payload;
    let result = {exitCode: exit_code, stdout};

    if (cmdListeners[id]) {
        let fn = cmdListeners[id];
        delete cmdListeners[id];
        fn(result);
    } else {
        cmdQueue[id] = result;
    }
});


/// Execution environment for agents

export const genv = {
    readFile, writeFile, listDir,
    fetch, cmd,
};

// Creates a function with the environment available in its body
export async function executeCode (body, params) {
    let fparams = [
        ... Object.entries(genv),
        ... Object.entries(params),
    ];

    console.log("code", body);
    console.log("function params", Object.fromEntries(fparams));

    // Don't let async errors leak
    let promises = [];
    function wrap (val) {
        if (val instanceof Function) {
            return function (...args) {
                let result = val(...args);
                if (result instanceof Promise) {
                    promises.push(result);
                }
                return result;
            }
        } else {
            return val;
        }
    }

    if (body.includes("await")) {
        body = `return (async ()=>{\n${body}\n})()`
    }

    // Pass the argument names, then the body
    let fn = new Function(...fparams.map(a=>a[0]), body);
    let result = fn(...fparams.map(
        a=>wrap(a[1])
    ));

    // Just wait for them
    let rawResults = await Promise.all(promises);

    if (result instanceof Promise) {
        result = await result;
    }

    return result;
}
