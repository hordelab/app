
import Plugin from "./Plugin";
import Script from "./Script";

import * as horde from "../horde";

let actions = {
    google: {
        inputs: [{
            name: 'query',
        }],
    }
};

// discriminated on the type
let schemas = {
    "message": {
        inputs: [
            {
                name: 'from',
                format: 'enum',
                values: ['system', 'user', 'agent', 'input']
            },
            {name: 'content', format: 'long'}
        ],
    },
    "infer-message": {
        inputs: [{
            name: 'from',
            format: 'enum',
            values: ['system', 'user', 'agent']
        }],
        outputs: [{name: 'content'}]
    },
    "clear": {
        inputs: [],
    },


    "chat-write": {
        inputs: [
            {name: 'content', format: 'long'},
            {name: 'format', format: 'enum', values: ['', 'md']}
        ]
    },
    "chat-event": {
        inputs: [
            {name: 'content', format: 'long'},
            {name: 'format', format: 'enum', values: ['', 'md', 'choice']}
        ]
    },
    "chat-read": {
        inputs: [],
        outputs: [{name: 'content'}]
    },
    "glm-setup": {
        inputs: [
            {name: 'model', format: 'enum', values: [
                'gpt-3.5-turbo'
            ]},
        ]
    },

    "glm-save": {
        outputs: [{name: 'snapshot'}]
    },
    "glm-restore": {
        inputs: [{name: 'snapshot'}]
    },


    // Branching
    "if": {
        inputs: [{name: 'condition'}],
        branches: ['true', 'false'],
    },

    // Some action nodes
    "javascript": {
        inputs: [
            {name: 'script', format: 'long', constant: true},
        ]
    },
    "expression": {
        inputs: [
            {name: 'expression', format: 'long'},
        ],
        outputs: [{name: 'result'},],
    },
    "template": {
        inputs: [
            {name: 'template', format: 'long', constant: true},
        ],
        outputs: [{name: 'result'}],
        computed: (inputs) => {
            //let t = template.parse(inputs.template);
            console.log("template.computed", inputs);
            return {};
        }
    },
    "file-read": {
        inputs: [{name: 'path'}],
        outputs: [{name: 'content', format: 'long'}],
    },
    "file-write": {
        inputs: [{name: 'path'}, {name: 'content', format: 'long'}],
    },
    "dir-list": {
        inputs: [{name: 'path'}],
    },

    "plugin": {
        inputs: [],
        async computed (inputs) {
            let schema = {
                inputs: [{
                    name: 'plugin',
                    format: 'enum',
                    values: Plugin.listReactive().map(p=>p.id),
                }],
                outputs: [],
            };

            if (inputs.plugin != null) {
                let plugin = await Plugin.get(inputs.plugin);

                schema.inputs.push({
                    name: 'member',
                    format: 'enum',
                    values: plugin.members.map(m => m.name),
                });

                if (inputs.member != null) {
                    let member = plugin.members.find(
                        m => m.name == inputs.member
                    );
                    if (member) {
                        schema.inputs.push(...member.inputs);
                        schema.outputs.push(...member.outputs);
                    }
                }
            }

            return schema;
        },
    },
    "script": {
        inputs: [],
        async computed (inputs) {
            let schema = {
                inputs: [{
                    name: 'script',
                    format: 'enum',
                    values: Script.listReactive().map(p=>p.id),
                }],
                outputs: [],
            };

            if (inputs.script != null) {
                let script = await Script.get(inputs.script);

                console.log("Script Schema", script);

                for (let slot of script.slots) {
                    if (slot.isInput) {
                        schema.inputs.push({
                            name: slot.key,
                        });
                    }

                    if (slot.isOutput) {
                        schema.outputs.push({
                            name: slot.key,
                        });
                    }
                }
            }

            return schema;
        },
    },
};

class Schema {
    static async compute (key, _inputs) {
        let schema = await Schema.get(key);
        console.log()

        let inputs = {};
        for (let k in _inputs) {
            inputs[k] = _inputs[k].value;
        }

        if (schema.computed) {

            schema = Object.assign({}, schema);
            let computed = await schema.computed(inputs);

            if (computed.inputs) {
                schema.inputs = schema.inputs ?
                    Array.from(schema.inputs) : [];
                schema.inputs.push(...computed.inputs);
            }

            if (computed.outputs) {
                schema.outputs = schema.outputs ?
                    Array.from(schema.outputs) : [];
                schema.outputs.push(...computed.outputs);
            }
        }

        return schema;
    }
    static async get (key) {
        let list = await this.list();

        let entry = list.find(it => it.key == key);
        return entry ? entry.value : {};
    }
    static async list () {
        if (this._list) {
            return this._list;
        }

        // Create list
        // TODO: Proper JSON Schemas

        let res = await horde.handle("system/actions");

        let list = [];
        for (let key in res.actions) {
            let schema = res.actions[key];
            list.push({
                key,
                value: {
                    inputs: schema.inputs ?
                        Object.entries(schema.inputs).map(([key, value]) => ({
                            name: key,
                            format: value,
                        })) : [],

                    outputs: schema.outputs ?
                        Object.entries(schema.outputs).map(([key, value]) => ({
                            name: key,
                            format: value,
                        })) : [],
                }
            });
        }

        console.log(list);
        return this._list = list;

        //return Object.entries(schemas).map(([key,value])=>({key,value}));
    }
}

export default Schema;
