import Model from "../hmodel";

import Task from "./Task";
import Script from "./Script";

class Agent extends Model {
    declare name: string;
    declare prompt: string;
    declare scriptId;

    /*constructor () {
        super()
        console.log('new agent', this);
    }*/

    // Class properties present in the schema must use keyword 'declare'
    static schema = {
        properties: {
            name: {type: "string"},
            prompt: {type: "string"},
            script: {type: "ref", path: ".horde/scripts"},
        }
    };

    get title () {
        return this.name || `Agent #${this.id}`;
    }

    async start (scriptId) {
        if (scriptId == null) {
            scriptId = this.scriptId;
            if (scriptId == null) {
                throw new Error("Missing script from argument or from agent")
            }
        }

        let script = await Script.get(scriptId);

        let task = await Task.create();
        task.scriptId = scriptId;
        task.agentId = this.id;

        return task;
    }
}

Agent.configure({
    path: ".horde/agents",
});

export default Agent;