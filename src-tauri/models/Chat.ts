import Model from "../hmodel";
import * as horde from "../horde";
import {reactive} from "vue";

type EventID = number;
type ObjectRef = string | number | null;
type ContentFormat = string | null;

export class Event {
    // Index to event
    previous: EventId;
    content: string | horde.Stream;
    format: ContentFormat;

    constructor (public content) {}
}

export class Message extends Event {
    sender: string;
    type: "message" = "message";

    constructor (
        public sender,
        content,
    ) {
        super(content)
    }
}

export class Chat extends Model {
    declare name: string;
    declare events: Event[];
    declare head: EventID | null;

    // ["main"] contains the main route of conversation
    //declare tags: Map<string, EventID>;

    declare agentId: ObjectRef;
    declare taskId: ObjectRef;

    main (): Event | null {
        let index = this.tags["main"]
        if (index != null) {
            return this.events[index];
        } else {
            return null;
        }
    }

    addEvent (content): Message {
        let msg = new Event(content);
        msg.previous = this.head;

        let eventId = this.events.length;
        reactive(this).events.push(msg);
        reactive(this).head = eventId;

        return msg;
    }

    addMessage (sender, content: string | horde.Stream): Message {
        let msg = new Message(sender, content);
        msg.previous = this.head;

        let r_this = reactive(this);

        let eventId = this.events.length;
        r_this.events.push(msg);
        r_this.head = eventId;

        return msg;
    }
}

Chat.configure({
    path: ".horde/chats",
    fields: {
        events (values) {
            return values.map(data => {
                return Object.assign(
                    data.type == "message" ?
                        new Message() :
                        new Event(),
                    data
                );
            });
        }
    }
});

export default Chat;
