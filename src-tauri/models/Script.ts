import Model from "../model";

export class Op {
    type: string;
}

export class Step {
    title: "";
    inputs: Record<String, Op[]> = {};
    outputs: Record<String, Op[]> = {};
    constructor (public id, public type) {}
}

export class Script extends Model {
    title = "";
    steps = [];
    values = [];
    slots = [];
    startStep = null;

    newStep (type) {
        let step = new Step(this.steps.length, type);
        step.id = this.steps.length;
        this.steps.push(step);
        return step;
    }

    copyFrom (other) {

        function clone1 (obj) {
            if (!(obj instanceof Object)) return obj;
            return Object.fromEntries(Object.entries(obj));
        }

        function clone2 (obj) {
            if (!(obj instanceof Object)) return obj;
            return Object.fromEntries(
                Object.entries(obj).map(
                    ([k, v]) => [k, clone1(v)]
                )
            );
        }

        this.title = other.title;
        this.slots = other.slots.map(clone1);
        this.steps = other.steps.map(step => {
            if (!step) return step;

            let newStep = clone1(step);
            newStep.inputs = clone2(step.inputs);
            newStep.outputs = clone2(step.outputs);

            return newStep;
        });
        this.startStep = other.startStep;
    }
}

Script.configure({
    path: "scripts",
    fields: {
        steps (steps) {
            for (let i = 0; i < steps.length; i++) {
                if (steps[i])
                    steps[i].id = i;
            }
            return steps;
        }
    },
});

export default Script;