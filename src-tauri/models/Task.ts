import Model from "../hmodel";

import Chat from "./Chat";

class Task extends Model {
    /*slots = [];
    messages = [];
    logs = [];
    currentStepPath = [];*/

    get title () {
        return "Task #" + this.id;
    }

    async getChat () {
        return this.chatId != null ? await Chat.get(this.chatId) : null;
    }

    async createChat () {
        let chat = Object.assign(await Chat.create(), {
            name: "Task " + this.id,
            taskId: this.id,
            agentId: this.agentId,
        });

        this.chatId = chat.id;
        return chat;
    }

    log (obj) {
        if (typeof obj == "string") {
            obj = {message: obj};
        }

        this.logs.push(obj);
        return obj;
    }
}

Task.configure({
    path: ".horde/tasks",
    default: {
        slots: [],
        logs: [],
    }
});

export default Task;