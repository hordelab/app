import Model from "../model";

import * as app from "../app";
import * as env from "../env";

class Plugin extends Model {
    content: string;
    members = [];

    get title () {
        // isFinite tests if the can value a valid number
        if (isFinite(this.id)) {
            return "Plugin #" + this.id;
        }
        return this.id;
    }
}

Plugin.configure({
    path: "plugins",
    extension: "js",
    parse (fileContent, id) {
        let plugin = new Plugin();
        plugin.content = fileContent;

        try {
            let fenv = Object.entries(env.genv);
            let fn = new Function(
                ...fenv.map(p=>p[0]),
                fileContent
            );
            let data = fn ? fn(...fenv.map(p=>p[1])) : {};

            let schema = data.schema || {};
            delete data.schema;

            for (let key in data) {
                let member = {
                    name: key,
                    call: data[key]
                };
                if (schema[key]) {
                    Object.assign(member, schema[key]);
                }
                plugin.members.push(member);
            }

            console.log("Plugin", plugin);
        } catch (e) {
            app.alert(`Error loading plugin ${id}: ${e.toString()}`);
            console.error("Error loading plugin", e);
        }

        return plugin;
    }
});

export default Plugin;