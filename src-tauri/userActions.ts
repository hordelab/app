
import Model from './model';

let stack = [];

export function log (action) {
  stack.push(action);
}

export function undo () {
  if (!stack.length) return;

  let action = stack.pop();
  if (action.undo) {
    action.undo();
  } else {
    console.log("Action has no undo", action);
  }
}