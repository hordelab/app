
import {handle} from "./horde";


var opening = false;

const config = {
    projectPath: null as string | null,
};

var listeners = {
    projectPath: [],
};



async function get (key) {
    let res = await handle("config/read", {key});
    console.log("config.%s", key, res)
    return res;
}

export async function set (key, value) {
    let res = await handle("config/write", {key, value});
    console.log("set config.%s", key, res, value)

    for (let fn of (listeners[key] || [])) {
        fn(value)
    }
}

export function watch (key, fn) {
    (listeners[key] ||= []).push(fn);
    get(key).then(val => val !== undefined && fn(val));
}





async function saveAll () {
    if (opening) return;
    console.log("save config", config);
}