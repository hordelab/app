
import { reactive } from "vue";

import Script from "./models/Script";
import Task from "./models/Task";
import Chat from "./models/Chat";
import Agent from "./models/Agent";
import Plugin from "./models/Plugin";

import Schema from "./models/Schema";

import * as template from "./template";
import * as glm from "./glm";
import * as env from "./env";
import * as app from "./app";

import * as horde from "./horde";

const queue = [];

var activeScript = null;
var activeTask = null;
var cancelled = false;

// Tasks waiting for an event
// key: task id
// value.task: task instance
// value.event: name of the event its waiting for
// value.slotKey: optional slot to save the event data
var waitMap = {};

async function getChat () {
    if (activeTask.chatId != null) {
        return await activeTask.getChat();
    } else {
        return await activeTask.createChat();
    }
}

function waitEvent (event) {
    let entry = waitMap[activeTask.id];
    if (entry) {
        if (entry.done) {
            delete waitMap[activeTask.id];
            return entry;
        } else {
            return null;
        }
    } else {
        waitMap[activeTask.id] = {task: activeTask, event, done: false};
        return null;
    }
}

export function dispatchEvent (event, taskId, value) {
    console.log("dispatch", event, taskId, value)
    let entry = waitMap[taskId];
    if (!entry) {
        console.error("Task %s is not waiting for events", taskId);
        return;
    }

    if (entry.event != event) {
        console.error("Task %s is not waiting for event %s", taskId, event);
        return;
    }

    entry.done = true;
    if (value) { entry.value = value; }

    start(entry.task);
}

async function executeStep () {
    if (!activeTask || !activeScript) {
        activeTask = null;
        activeScript = null;
        return;
    }

    // Declare State

    let step = activeScript.steps[activeTask.currentStep];
    console.log("Task %s. Step %s [%s]", activeTask.title, step.title, step.type)

    let doAdvance = true;
    let doSchedule = true;



    // Declare Functions

    function readInput (key) {
        let inputDef = step.inputs[key];

        if (!inputDef) {
            console.warn("Step %s is missing input %s", step.title, key, activeScript);
            return "";
        }

        if (typeof inputDef == "string") {
            return inputDef;
        } else if (inputDef.type == "constant" || !inputDef.type) {
            return inputDef.value;
        } else if (inputDef.type == "slot") {
            let slot = activeTask.slots.find(s => s.key == inputDef.key);
            if (!slot || !slot.value) {
                console.warn("Slot %s has no value", inputDef.key);
            }
            return slot && slot.value;
        } else {
            console.error("Unknown step input", inputDef);
        }
    }

    function writeOutput (key, value) {
        console.log('writeOutput', key, value);

        if (cancelled || !step.outputs) return;
        let def = step.outputs[key];
        if (!def) return;

        let slotKey = def.key;

        let slot = activeTask.slots.find(s => s.key == slotKey);
        if (slot) {
            slot.value = value;
        } else {
            activeTask.slots.push({key: slotKey, value});
        }

        if (value instanceof horde.Stream) {
            let log = activeTask.log({
                tag: 'slot set',
                message: `${slotKey} = (stream)`
            });
            value.on('data', delta => { log.message += delta; });
        } else {
            activeTask.log({
                tag: 'slot set',
                message: `${slotKey} = ${JSON.stringify(value)}`
            });
        }
    }

    function error (err) {
        let msg = err.toString ? err.toString() : String(err);

        activeTask.log({
            tag: 'error',
            message: msg,
            data: {
                error: err,
                step: activeTask.currentStep,
            }
        });
        app.alert(`Task ${activeTask.title} Error: ${msg}`)
        console.error(err);
        cancelled = true;
    }

    async function createEnv () {
        let slots = Object.fromEntries(activeTask.slots.map(
            slot => [slot.key, slot.value]
        ));

        let agent = activeTask.agentId == null ? null :
            await Agent.get(activeTask.agentId);

        return {
            slots,
            agent: agent && {
                name: agent.name,
                prompt: agent.prompt,
            }
        }
    }



    // Execute

    let taskPath = Task.idToPath(activeTask.id);
    console.log('load task...');
    await horde.handle('task/load', { taskId: taskPath })
    console.log('task loaded!');

    try {
        switch (step.type) {
            case "message": {
                let from = readInput('from');
                let content = readInput('content');

                if (content instanceof horde.Stream) {
                    content = await content.getContent();
                }

                if (from == "agent") from = "assistant";

                await horde.handle("chatgpt/add", {role: from, content});
                activeTask.log({
                    tag: 'llm message',
                    message: `${from}: ${content}`
                });

                break;
            }
            case "infer-message": {
                let from = readInput('from')

                let log = activeTask.log({
                    tag: 'llm infer',
                    message: "",
                });

                let res = await horde.handle("chatgpt/infer");
                let stream = new horde.Stream(res.streamId);

                stream.on('data', delta => {
                    log.message += delta;
                });

                writeOutput('content', stream);
                break;
            }
            case "clear": {
                glm.clear();
                activeTask.log({
                    tag: 'chat clear'
                });
                break;
            }


            case "glm-save": {
                let snapshotId = glm.save();
                writeOutput('snapshot', snapshotId);
            }
            case "glm-restore": {
                let snapshotId = readInput('snapshot');
                glm.restore(snapshotId);
            }


            case "chat-read": {
                let chat = await getChat();

                activeTask.log({
                    tag: 'chat read',
                });

                let result = waitEvent('chat');
                if (result) {
                    writeOutput('content', result.value);
                    activeTask.logs[activeTask.logs.length-1].message = result.value;
                } else {
                    doAdvance = false;
                    doSchedule = false;
                }
                break;
            }
            case "chat-write": {
                let chat = await getChat();

                let from = 'agent';
                let content = readInput('content');
                let format = readInput('format');

                activeTask.log({
                    tag: 'chat write',
                    message: content,
                });

                console.log("chat write", content);

                let msg = chat.addMessage(from, content);
                msg.format = format;
                break;
            }
            case "chat-event": {
                let chat = await getChat();

                let content = readInput('content');
                let format = readInput('format');

                /*activeTask.log({
                    tag: 'chat write',
                    message: content,
                });*/
                let event = chat.addEvent(content);
                event.format = format;
                break;
            }



            case "javascript": {
                let script = readInput('script');
                let tenv = await createEnv();

                try {
                    let result = await env.executeCode(script, tenv);
                } catch (e) {
                    error("Error executing script: " + e.toString());
                    break;
                }

                // Apply changes done to the slots
                for (let {key} of activeScript.slots) {
                    let slot = activeTask.slots.find(s => s.key == key);
                    let newval = tenv.slots[key];

                    if (slot && slot.value != newval) {
                        // This might become a heavier operation at some point
                        // so it's worth leaving the diff check.
                        // Better a setter tho
                        slot.value = newval;
                    } else if (!slot && newval !== undefined) {
                        activeTask.slots.push({
                            key, value: newval
                        });
                    }
                }

                break;
            }

            case "expression": {
                let script = `return (${readInput('expression')})`;
                let tenv = await createEnv();

                let result;
                try {
                    result = await env.executeCode(script, tenv);
                } catch (e) {
                    error("Error executing script: " + e.toString());
                    break;
                }

                writeOutput('result', result);
                break;
            }

            case "template": {
                let slots = Object.fromEntries(activeTask.slots.map(
                    slot => [slot.key, slot.value]
                ));

                let raw = readInput('template');
                let temp = template.parse(raw);
                let result = temp.render(slots);

                writeOutput('result', result);
                break;
            }

            case "if": {
                let expr = readInput('condition');
                let slots = Object.fromEntries(activeTask.slots.map(
                    slot => [slot.key, slot.value]
                ));
                let script = "return (" + expr + ");"

                try {
                    let result = await env.executeCode(script, {slots});

                    let branchKey = result ? "true" : "false";

                    let nextStep = step.next[branchKey];
                    if (nextStep) {
                        activeTask.currentStep = nextStep;
                    } else {
                        doSchedule = false;
                    }

                    doAdvance = false;
                } catch (e) {
                    error("Error executing script: " + e.toString());
                    break;
                }

                break;
            }

            case "plugin": {
                let pluginId = readInput('plugin');
                if (pluginId == null) {
                    error("Missing plugin parameter");
                    break;
                }

                let memberName = readInput('member');
                if (memberName == null) {
                    error("Missing member parameter");
                    break;
                }

                let plugin = await Plugin.get(pluginId);
                let member = plugin.members.find(m => m.name == memberName);

                if (!member) {
                    error(`Plugin member ${memberName} not found`);
                    break;
                }

                let inputs = Object.fromEntries(
                    member.inputs.map(schema => {
                        let key = schema.name;
                        return [key, readInput(key)];
                    })
                );

                console.log("Run plugin %s.%s", pluginId, memberName, inputs);
                try {
                    let result = member.call(inputs) || {};
                    if (result instanceof Promise) {
                        result = await result;
                    }

                    console.log("result", result);
                    for (let schema of member.outputs) {
                        let key = schema.name;
                        if (result[key] !== undefined) {
                            writeOutput(key, result[key]);
                        }
                    }
                } catch (e) {
                    error(`${plugin.title} member ${memberName} failed: ${e.toString()}`);
                }

                break;
            }

            case "script": {
                let scriptId = readInput('script');
                if (scriptId == null) break;

                let result = waitEvent('done');
                if (result) {
                    // Listener called

                    let task = await Task.get(result.value.taskId);
                    let script = await Script.get(scriptId);

                    for (let slot of script.slots) {
                        if (slot.isOutput) {
                            let slotVal = task.slots.find(s => s.key == slot.key);
                            console.log("script output", slot, slotVal);
                            if (slotVal) {
                                writeOutput(slot.key, slotVal.value);
                            }
                        }
                    }

                    activeTask.log({
                        tag: 'script ' + scriptId,
                        message: 'Done'
                    });
                } else {
                    // Added listener

                    let script = await Script.get(scriptId);
                    if (!script) {
                        console.error("Script doesn't exist");
                        break;
                    }

                    activeTask.log({
                        tag: 'script ' + scriptId,
                        message: `Start ${script.title}`
                    });

                    let task = await Task.create();
                    task.scriptId = script.id;
                    task.parent = activeTask.id;

                    for (let slot of script.slots) {
                        if (slot.isInput) {
                            task.slots.push({
                                key: slot.key,
                                value: readInput(slot.key),
                            });
                        }
                    }

                    console.log("Starting task", task);
                    start(task);

                    doAdvance = false;
                    doSchedule = false;
                }
                break;
            }


            default: {
                let schema = await Schema.get(step.type);

                let inputs = {};
                for (let {name: key} of schema.inputs) {
                    inputs[key] = readInput(key);
                }

                console.log('handle step', step.type);

                let res = await horde.handle(step.type, inputs);
                console.log(step.type, inputs, res);

                for (let {name: key} of schema.outputs) {
                    console.log("output", key, res[key]);
                    writeOutput(key, res[key]);
                }
            }
        }
    } catch (e) {
        console.error("Error executing step", e)
    }



    // Cleanup and next step

    await horde.handle('task/load', { taskId: null })

    if (!cancelled) {
        // Advance or end
        if (doAdvance) {
            if (step.next == null) {
                doSchedule = false;
                activeTask.currentStep = null;

                if (step.parent) {
                    // trigger event(parent, 'done');
                }
            } else {
                activeTask.currentStep = step.next;
            }
        }

        if (activeTask.currentStep) {
            let nextStep = activeScript.steps[activeTask.currentStep];
            activeTask.log({
                tag: "step " + activeTask.currentStep,
                message: nextStep && nextStep.title,
            });
        }
    }

    if (doSchedule && !cancelled) {
        scheduleNextStep();
    } else {
        if (activeTask.parent != null) {
            dispatchEvent('done', activeTask.parent, {
                taskId: activeTask.id
            });
        }

        activeTask = null;
        activeScript = null;
        cancelled = false;

        if (queue.length > 0) {
            start(queue.shift());
        }
    }
}

function scheduleNextStep () {
    requestAnimationFrame(executeStep);
}

export async function start (task) {
    console.log("scheduling task " + task.id);

    if (!activeTask) {
        if (!task instanceof Object) {
            let err = new Error("Task is not an object", task);
            console.log(err);
            throw err;
        }
        activeTask = reactive(task);
        activeScript = await Script.get(task.scriptId);

        if (task.currentStep == null) {
            // Start the task if it hasn't started
            activeTask.currentStep = activeScript.startStep;

            if (task.currentStep == null) {
                console.error("Script has no start step", activeScript);
                return;
            }
        }

        console.log({steps: activeScript.steps, currentStep: task.currentStep});

        console.log("Start %s at step #%d %s",
            task.title, task.currentStep, activeScript.steps[task.currentStep].title);

        scheduleNextStep();
    } else {
        console.log("Queue %s in position %d", task.title, queue.length);
        queue.push(task);
    }
}

export async function cancel (task) {
    if (!activeTask || cancelled) return;

    if (activeTask.id == task.id) {
        cancelled = true;
        await glm.cancel();
    } else {
        let index = queue.indexOf(task);
        if (index < 0) {
            console.log("Task was not executing" + task.id);
        } else {
            console.log("cancelled task " + task.id);
            queue.splice(index, 1);
        }
    }
}

export function getTaskStatus (task) {
    if (activeTask && task.id == activeTask.id) {
        return "running";
    } else if (waitMap[task.id] != null) {
        return "waiting";
    } else {
        return "none";
    }
}