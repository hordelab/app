
//import Base from "./glm/llamars";
import Base from "./glm/chatgpt";

Base.setup();

export function clear () {
  Base.clear();
}

export async function addMessage (role, message) {
  await Base.addMessage(role, message);
}

export async function send (callback) {
  return await Base.send(callback);
}

export async function cancel () {
  return await Base.cancel();
}

export function save () {
  return Base.save();
}

export function restore (snapshotId) {
  Base.restore(snapshotId);
}

