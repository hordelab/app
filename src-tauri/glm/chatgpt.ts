
import * as config from "../config";
import {handle, readStream} from "../horde";

const snapshots = [];

const ChatGPT = {

  setup () {
  },

  clear () {
    handle("chatgpt/clear");
  },

  async addMessage (role, content) {
    if (role == "agent") role == "response";
    await handle("chatgpt/add", {role, content});
  },

  cancel () {
    throw new Error("ChatGPT Cancel Not Implemented")
  },

  async send (callback) {
    let res = await handle("chatgpt/infer");
    console.log("ChatGPT send: ", res)

    return await new Promise(resolve => {
      let result = ""

      function handleChunk (chunk) {
        callback(chunk);
        result += chunk;
      }

      function handleEof () {
        resolve(result);
      }

      readStream(res.streamId, handleChunk, handleEof);
    });
  },

  save () {
    let res = handle("chatgpt/save", {role, content});
    return res.id;
  },

  restore (snapshotId) {
    handle("chatgpt/restore", {id: snapshotId});
  }
};

export default ChatGPT;