import getConfig from "./appconfig";

// Used as default. Pass the host url to Horde constructor instead.
const HORDE_URL = "ws://localhost:17922";

// Time to wait when a request doesn't go through, in milliseconds.
const RETRY_TIME = 300;

const configPromise = getConfig();

var _root_horde = null;

function decodeNet (value) {
    if (value instanceof Array) {
        return value.map(decodeNet);
    }
    if (value instanceof Object) {
        if (value['$horde'] == 'stream') {
            return new Stream(_root_horde, value.id);
        }
        return Object.fromEntries(
            Object.entries(value).map(
                ([k, v]) => [k, decodeNet(v)]
            )
        );
    }

    // fallthrough
    return value;
}

function encodeNet (value) {
    if (value instanceof Stream) {
        return {
            '$horde': 'stream',
            'id': value.id,
        };
    }
    if (value instanceof Array) {
        return value.map(encodeNet);
    }
    if (value instanceof Object) {
        return Object.fromEntries(
            Object.entries(value).map(
                ([k, v]) => [k, encodeNet(v)]
            )
        );
    }

    // fallthrough
    return value;
}

export class Stream {
    constructor (horde, id) {
        this.horde = horde;
        this.id = id;
        this.partialContent = "";
        this.eof = false;

        this.listeners = {
            data: [],
            end: [],
        };

        this.horde.sendWs({
            path: 'stream',
            body: {id: this.id},
            onResponse: (msg) => {
                if (this.eof) return;

                if (msg.eof) {
                    this.eof = true;
                    this.handle('end');
                } else {
                    this.partialContent += msg.data;
                    this.handle('data', msg.data);
                }
            }
        });

        this.contentPromise = new Promise((resolve, reject) => {
            this.handle("end", () => {
                resolve(this.partialContent);
            });
        });
    }

    getContent () {
        return this.contentPromise;
    }

    handle (event, ...args) {
        for (let fn of this.listeners[event]) {
            fn(...args)
        }
    }

    on (event, callback) {
        this.listeners[event].push(callback);

        if (event == "data" && this.partialContent) {
            callback(this.partialContent);
        }
        if (event == "end" && this.eof) {
            callback();
        }
    }
}

class Instance {
    horde = null
    model = null
    handle = null

    constructor (horde, model, handle) {
        this.horde = horde;
        this.model = model;
        this.handle = handle;
    }

    async get () {
        let res = await this.horde.send(`/m/${this.model}`, {
            args: {handle: this.handle},
        });
        return res.value;
    }

    async update (subpath, value) {
        let res = await this.horde.send(`/m/${this.model}`, {
            args: {
                handle: this.handle,
                subpath: subpath
                    .map(s => String(s).replace('.', '\\.'))
                    .join('.'),
            },
            method: 'POST',
            body: value
        });
        return res;
    }

    // Deletes the instance in the server
    async remove () {
        console.log("Delete", this.handle)
        let res = await this.horde.send(`/m/${this.model}/delete`, {
            args: {
                handle: this.handle,
            },
            method: 'POST',
        });
        // res.success == true
    }

    observe (callback) {
        let sendRequest = () => {
            console.log("observe request", this.model, this.handle);

            this.horde.sendWs({
                path: `/m/observe`,
                body: {
                    model: this.model,
                    handle: this.handle,
                },
                onResponse (msg) {
                    callback(msg.subpath, decodeNet(msg.value));
                }
            });
        };
        this.horde.closeListeners.push(sendRequest);
        sendRequest();
    }
}

class Horde {
    constructor (host = HORDE_URL) {
        this.host = host;

        if (_root_horde == null)
            _root_horde = this;

        this.lastConnection = new Date();
        this.websocketReady = false;
        this.websocketNonce = 0;
        this.websocketQueue = [];
        this.nonceListeners = {};
        this.closeListeners = []
        this.connectWebsocket();
    }

    async connectWebsocket () {
        console.log("connectWebsocket", Boolean(this.ws));
        if (this.ws) return;

        const wsUrl = this.host.replace(/^http/, 'ws').replace(/^https/, 'wss') + '/ws';
        console.log("Connect WebSocket to ", wsUrl);
        this.ws = new WebSocket(wsUrl);
        this.websocketReady = false;

        this.ws.onopen = () => {
            // Make lastConnection be the last reconnection attempt
            this.lastConnection = new Date();
            this.configureWebsocket();

            this.websocketReady = true;
            for (let callback of this.websocketQueue) {
                callback();
            }
            this.websocketQueue = [];
        };

        this.ws.onmessage = async (event) => {
            const data = JSON.parse(event.data);
            console.log("onmessage", data);
            this.receiveWebsocketMessage(data);
        };

        this.ws.onerror = (error) => {
            console.error('WebSocket error:', error);
        };

        this.ws.onclose = () => {
            console.log("Websocket connection closed");

            // If the websocket was properly connected,
            // set the last connection to the disconnection time.
            // lastConnection is used to time when is best appropriate
            // to reconnect, so keeping it recent helps with responsiveness.
            if (this.websocketReady) {
                this.lastConnection = new Date();
            }

            this.ws = null;
            this.websocketReady = false;

            for (let callback of this.closeListeners) {
                callback();
            }

            const elapsed = new Date() - this.lastConnection;
            const delay = Math.max(500, elapsed / 2);

            setTimeout(() => {
                this.connectWebsocket();
            }, delay);
        };
    }

    async configureWebsocket () {
        let config = await configPromise;

        if (config.token) {
            let res = this.sendWs({
                path: "auth",
                body: {token: config.token},
                nonce: false,
                forced: true,
            });
        }
    }

    async sendWs (msg) {
        let resultPromise;

        let forced = Boolean(msg.forced);
        if (msg.forced !== undefined) {
            delete msg.forced;
        }

        if (msg.onResponse) {
            const callback = msg.onResponse;
            const nonce = msg.nonce = this.websocketNonce++;
            delete msg.onResponse;

            resultPromise = new Promise((resolve, reject) => {
                this.nonceListeners[nonce] = {
                    resolve: (msg) => {
                        resolve();
                        callback(msg);
                    },
                    reject
                };
            });
        } else if (msg.nonce != null) {
            const nonce = msg.nonce = this.websocketNonce++;

            resultPromise = new Promise((resolve, reject) => {
                this.nonceListeners[nonce] = {
                    resolve: (msg) => {
                        resolve(msg);
                        delete this.nonceListeners[nonce];
                    },
                    reject
                };
            });
        }

        if (forced) {
            this.ws.send(JSON.stringify(msg));
            return;
        }

        // Trying sending until success.
        let retry_time = RETRY_TIME;

        while (true) {
            if (!this.websocketReady) {
                await new Promise(resolve => {
                    this.websocketQueue.push(resolve)
                })
            }

            try {
                this.ws.send(JSON.stringify(msg));
            } catch (e) {
                console.error(e);
                // Wait for a period
                await new Promise(resolve => {
                    setTimeout(retry_time, resolve);
                })

                // Increase the retry time by half it's current value
                retry_time = (retry_time * 1.5) | 0;

                // Don't let this iteration continue.
                continue;
            }

            if (resultPromise) {
                return await resultPromise;
            }

            // Break the send loop if no response is expected
            break;
        }
    }

    async receiveWebsocketMessage (msg) {
        if (msg.nonce != null) {
            let listener = this.nonceListeners[msg.nonce];

            if (listener) {
                delete msg.nonce;

                if (msg.error) {
                    listener.reject(msg.error);
                } else {
                    listener.resolve(msg);
                }
            }
        }
    }

    async makeHeaders (params) {
        let headers = {};

        if (params && params.body !== undefined) {
            headers['Content-Type'] = 'application/json';
        }

        return headers;
    }

    async handle (method, args = {}) {
        const response = await this.sendWs({
            path: "h/" + method,
            body: encodeNet(args),
            nonce: true,
        });

        return decodeNet(response.output);
    }

    async send (path, params={}) {
        let query = "";

        let config = await configPromise;
        if (config.token) {
            params.args = Object.assign({}, params.args || {}, {tk: config.token});
        }

        if (params.args) {
            query = "?" + Object.entries(params.args)
                .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
                .join('&');
        }

        if (path[0] == "/") {
            path = path.slice(1);
        }

        const url = `${this.host}/${path}${query}`;
        const response = await fetch(url, {
            method: params.method || "GET",
            headers: await this.makeHeaders({body: params.body !== undefined}),
            body: params.body !== undefined ? JSON.stringify(params.body) : undefined,
        });

        if (!response.ok) {
            console.error(response);
            try {
                let text = await response.text();
                throw new Error(`Request failed: ${text}`);
            } catch (e) {
                if (response.status == 401) {
                    throw new Error("Unauthorized");
                } else {
                    throw new Error(`Request failed with status ${response.status}`);
                }
            }
        }

        if (params.onData) {
            const reader = response.body.getReader();
            const decoder = new TextDecoder();

            try {
                while (true) {
                    const { done, value } = await reader.read();
                    if (done) {
                        break;
                    }

                    params.onData(
                        decoder.decode(value, { stream: true })
                    );
                }
            } finally {
                if (params.onClose) {
                    params.onClose();
                }
            }

        } else {
            const data = await response.json();
            return data;
        }
    }

    async modelIndex (model: string) {
        let res = await this.send(`/m/${model}/index`);
        return res.entries;
    }

    getStream (stream_id: string) {
        return new Stream(this, stream_id);
    }
}

let promise = (async function () {
    let config = await getConfig();
    let hostUrl = config.hostUrl;

    return new Horde(hostUrl);
})();

// Public Interface Functions

export async function getInstance (model: string, handle: string) {
    let horde = await promise;
    return new Instance(horde, model, handle);
}

export async function getIndex (model: string) {
    let horde = await promise;
    return horde.modelIndex(model);
}

export async function createInstance (model: string, members=null) {
    let horde = await promise;
    let res = await horde.send(`/m/${model}/create`, {
        method: 'POST',
        body: members ? {members} : undefined,
    });
    return new Instance(horde, model, res.id);
}

export async function login (username: string, password: string) {
    let horde = await promise;
    let res = await horde.send(`/user/login`, {
        method:'POST',
        body: {username, password},
    });
    return res.token;
}

export async function signup (email: string, username: string, password: string) {
    let horde = await promise;
    let res = await horde.send(`/user/signup`, {
        method:'POST',
        body: {email, username, password},
    });
    return res.token;
}

export async function getHorde () {
    return await promise;
}



// COMPATIBILITY

function extractPathInfo(str) {
    const indexRegex = /^\.horde\/([^\/]+)s$/;
    const instanceRegex = /^\.horde\/([^\/]+)s\/([^\/]+)\.json$/;

    const indexMatch = str.match(indexRegex);
    if (indexMatch) {
        return { model: indexMatch[1], isIndex: true };
    }

    const instanceMatch = str.match(instanceRegex);
    if (instanceMatch) {
        const [, model, id] = instanceMatch;
        return { model, id };
    }

    throw new Error('Not a valid object path:' + str);
}

export async function observeObject (path, callback) {
    let {model, id, isIndex} = extractPathInfo(path);

    let horde = await promise;

    if (isIndex) {
        // Not really observing, just hacked to return something useful.
        let index = await horde.modelIndex(model);
        callback(index);
    } else {
        return observeInstance(model, id, callback)
    }
}

export async function observeInstance (model, id, callback) {
    let object = {};

    function copy (value) {
        // Deep-copying horde objects is complex and not necessary.
        if (value instanceof Stream) {
            return value;
        } else if (value instanceof Array) {
            return value.map(copy);
        } else if (value instanceof Object) {
            return  Object.fromEntries(
                Object.entries(value).map(
                    ([k, v]) => [k, copy(v)]
                )
            );
        } else {
            return value;
        }
    }

    function updateFn (subpath, value) {
        if (typeof subpath == "string") {
            subpath = [subpath];
        }

        if (subpath == "*") {
            object = value;
        } else {
            let tgt = object;

            while (subpath.length > 1) {
                let key = subpath.shift();
                if (tgt[key] == null) {
                    tgt[key] = {};
                }
                tgt = tgt[key];
            }            

            let key = subpath[0];
            if (tgt instanceof Array && (key == 'add' || key == tgt.length)) {
                tgt.push(value);
            } else if (value == null) {
                if (tgt instanceof Array) {
                    tgt.splice(parseInt(key), 1);
                } else {
                    delete tgt[key];
                }
            } else {
                tgt[key] = value;
            }
        }

        // Necessary to do a deep copy of the object in order for Vue to
        // detect it as a change
        let copied = copy(object);
        callback(copied);
    }

    let horde = await promise;

    let instance = await getInstance(model, id);
    instance.observe(updateFn);
}


export async function observeIndex (rootPath, updateFn) {
    function pathToId (path) {
      if (path.startsWith(rootPath)) {
        path = path.slice(rootPath.length + 1);
      }
      if (path.endsWith(".json")) {
        path = path.slice(0, -5);
      }
      return path;
    }

    // returns promise of the cancel function
    return await observeObject(rootPath, obj => {
      let list = Object.entries(obj).map(entry => {
        let value = Object.assign({}, entry[1]);
        value.id = pathToId(entry[0]);
        return value;
      });

      updateFn(list);
    });
}

export async function handle (method, inputs) {
    let horde = await promise;
    let res = await horde.handle(method, inputs);
    return res;
}

