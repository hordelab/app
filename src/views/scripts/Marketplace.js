import * as horde from '../../horde';
import * as ui from '../../ui';

export async function fetchListings () {
    let res = (await horde.getHorde()).send('marketplace/index');
    return res;
}

export async function installListing (listingType, handle) {
    let h = await horde.getHorde()
    let data = await h.send(`marketplace/${listingType}/${handle}`);

    if (listingType == "agent") {
        let res = await h.handle(`agent/create`, {data});
        ui.goto(`/agent/${res.agent_id}`);
    } else if (listingType == "script") {
        let scriptId = (await horde.createInstance('script', data)).id;
        ui.goto(`/script/${scriptId}`)
    } else {
        ui.alert(`Installing '${listingType}' is not yet supported`);
    }
}
