<script setup lang="ts">
  import {
    ref, computed, reactive, watch,
    onMounted, onUnmounted,
    shallowRef, triggerRef,
  } from "vue";

  import * as horde from '../horde';
  import * as ui from '../ui';
  import {SchemaReader} from '../schema';

  import BoardStepField from '../components/BoardStepField.vue'

  const schemas = new SchemaReader();

  const props = defineProps<{
    id: string
  }>();

  const script = ref({
    nodes: [],
    logs: [],
  });

  const scriptVisibility = ref('private')

  watch(() => props.id, id => {

    // returns promise of the cancel function
    horde.observeObject(`.horde/scripts/${id}.json`, obj => {
      try {
        computeBoardItems(obj);
      } catch (error) {
        console.error("Could not load board", error);
      }

      scriptVisibility.value = ['private', 'protected'][obj.visibility || 0]
      script.value = obj;
    });
  }, {immediate: true});

  async function getScriptInstance () {
    return await horde.getInstance('script', script.value.id);
  }

  async function updateScript (subpath, value) {
    let instance =  await horde.getInstance('script', script.value.id);
    await instance.update(subpath, value);
  }

  watch(scriptVisibility, value => {
    let valueMap = {
      'private': 0,
      'protected': 1,
    };
    let intValue = valueMap[value];
    if (intValue == null) {
      console.error("Unknown visibility value: ", value);
    } else {
      updateScript(['visibility'], intValue);
    }
  })



  // === Board Navigation === //

  // Pixels per unit of scale
  const SCALE = 16;

  // Navigation State
  const nav = reactive({ x: 0, y: 0, zoom: 0 });

  function boardCssStyle () {
    return {
      transform: `translate(
        ${nav.x * SCALE}px,
        ${nav.y * SCALE}px
      ) scale(
        ${zoomScale()}
      )`
    };
  }

  function backgroundCssStyle () {
    return {
      'background-position': `${nav.x * SCALE}px ${nav.y * SCALE}px`
    };
  }


  function zoomScale () {
    return Math.pow(1.125, nav.zoom);
  }

  function viewScale () {
    return SCALE * zoomScale();
  }



  // === Board Items === //

  // TODO: I think this will hurt performance, as changing any node will force update all others
  const nodes = shallowRef([]);
  const edges = shallowRef([]);
  const variables = shallowRef({});

  let nodeMap = {};
  let nodeActiveCount = reactive({});

  //let _nodeNonces = Map();
  class Node {
    constructor (step, id) {
      this._step = step;

      this.id = id;
      this.index = id; // deprecated

      this.x = step.x || 0;
      this.y = step.y || 0;

      this.w = step.w || 10;
      this.h = step.h || 10;

      this.error = false;
      this.schema = {
        inputs: {},
        outputs: {},
      };

      this.vertices = [];

      this.schemaPromise = new Promise(resolve => {

        schemas.getAction(step.type).then(schema => {
          if (schema) {
            this.computeVertices(schema);
            this.schema = schema;
            resolve(schema);
          } else {
            this.error = true;
            console.error(`Step ${this.index} [${step.type}] has no schema`)
            return;
          }

          // TODO: have nodes contain a shallow ref to themselves,
          // then trigger that one for each update.
          // To avoid recomputing the shole node list with every change
          triggerRef(nodes);
        });

      });
    }

    cssStyle () {
      return {
        left: this.x * SCALE + 'px',
        top: this.y * SCALE + 'px',

        width: this.w * SCALE + 'px',
        height: this.h * SCALE + 'px',
      }
    }

    cssClass () {
      let list = [];
      if (this.error) {
        list.push('error');
      }

      if (script.value.startStep == this.index) {
        list.push('start');
      }

      if (nodeActiveCount[this.id]) {
        list.push('active');
      }

      return list;
    }

    get title () {
      return this._step.title;
    }

    get action () {
      // TODO: Step type should be called action
      return this._step.type;
    }

    move (dx, dy) {
      this.x += dx;
      this.y += dy;

      triggerRef(nodes);
    }

    resize (dx, dy) {
      this.w += dx;
      this.h += dy;

      triggerRef(nodes);
    }

    delete () {
      this.disconnectPrevious();
      this.update([], null);
    }

    async update (subpath, value) {
      updateScript(['nodes', this.id, ...subpath], value);
    }

    replaceAll (newData) {
      this.update([], newData);
    }

    makeStart () {
      updateScript(['startStep'], this.id);
    }

    connect (nextId, label) {
      let vertexPath = label ? ['next', label] : ['next'];
      this.update(vertexPath, nextId);
    }

    disconnectNext () {
      this.update(['next'], null);
    }

    disconnectPrevious () {
      for (let node of nodes.value) {
        if (node._step.next == this.id) {
          node.disconnectNext();
        }
      }
    }

    disconnectAll () {
      // Each disconnect is async
      this.disconnectNext();
      this.disconnectPrevious();
    }

    async duplicate () {
      let duplicateId = await createNode(this.action, {x: this.x, y: this.y + 3})

      let proms = [
        updateScript(['nodes', duplicateId, 'next'], this._step.next),
        updateScript(['nodes', duplicateId, 'inputs'], this._step.inputs),
        updateScript(['nodes', duplicateId, 'outputs'], this._step.outputs),
      ];

      await Promise.all(proms);
    }

    sendUpdates (subpaths) {
      for (let subpath of subpaths) {
        let value;

        if (subpath.length == 1) {
          let key = subpath[0];

          if (['x', 'y', 'w', 'h'].includes(key)) {
            value = this[key];
          } else {
            console.error('Unknown subpath key: ' + key);
            continue;
          }
        } else {
          console.error('Unknown subpath: ' + subpath);
          continue;
        }

        // Async
        this.update(subpath, value);
      }
    }

    computeVertices (schema) {
      if (schema.branches) {
        let branchCount = schema.branches.length;
        this.vertices = schema.branches.map(
          (branchKey, index) => new Vertex({
            node: this,
            label: branchKey,
            offset: (index + 1) / (branchCount + 1),
          })
        );
      } else {
        this.vertices = [new Vertex({
          node: this,
          label: null,
          offset: 0.5,
        })];
      }
    }
  }

  class Edge {
    constructor (start, end) {
      this.start = start;
      this.end = end;
    }

    svgPath () {
      if (!this.start) { return ""; }

      let a = {
        x: this.start.x * SCALE,
        y: (this.start.y + this.start.h) * SCALE
      };

      let b = this.end ? {
        x: this.end.x * SCALE,
        y: this.end.y * SCALE
      } : {x: a.x, y: a.y + SCALE*4};

      // Line
      //return `M ${a.x},${a.y} ${b.x},${b.y}`;

      // Cubic
      let dist = Math.sqrt(
        Math.pow(a.x - b.x, 2),
        Math.pow(a.y - b.y / SCALE, 2),
      );

      let d = dist/2;

      return `
        M ${a.x},${a.y}
        C ${a.x},${a.y+d}
          ${b.x},${b.y-d}
          ${b.x},${b.y}`;
    }
  }

  class Vertex {
    constructor (params={}) {
      this.node = params.node;
      this.label = params.label || null;
      this.offset = params.offset || 0.5;
    }

    connect (stepId) {
      this.node.connect(stepId, this.label);
    }

    cssStyle () {
      return {
        top: '100%',
        left: Math.round(this.offset * 100) + "%",
        cursor: 'pointer',
      };
    }

    get x () {
      return this.node.x + this.node.w * (this.offset - 0.5);
    }

    get y () {
      return this.node.y + this.node.h;
    }

    // TODO: Stop using height to calculate the connection
    get h () { return 0; }
  }

  function computeBoardItems (script) {
    nodes.value = Object.entries(script.nodes)
      .filter(pair => pair[1])
      .map(
        ([index, obj]) => new Node(obj, index)
      );

    nodeMap = {};
    for (let node of nodes.value) {
      nodeMap[node.index] = node;
    }

    let _edges = [];
    for (let node of nodes.value) {
      let next = node._step.next;

      if (next == null) continue;
      if (next instanceof Object) {
        for (let branchKey in next) {
          let nextNode = nodeMap[next[branchKey]];

          // At creation, edge doesn't know what vertex to use to start.
          // Wait for the node schema to load to compute the actual starting position.
          let edge = new Edge(node, nextNode);
          node.schemaPromise.then(_schema => {
            // Search for the vertex that matches the branchKey

            let vertex = node.vertices.find(v => v.label == branchKey);
            if (vertex) {
              edge.start = vertex;

              // TODO: Trigger internal edge ref, instead of recomputing all edges
              triggerRef(edges);
            } else {
              console.error(`Out edge ${branchKey} has no corresponding vertex in node ${node.id}`);
              // TODO: Remove edge
            }
          });

          _edges.push(edge);
        }
      } else {
        let nextNode = nodeMap[next];
        _edges.push(new Edge(node, nextNode));
      }
    }
    edges.value = _edges;

    // Sometimes it comes broken >:(
    // Filter out when a variable is null (should not happen)
    variables.value = script.variables.filter(x=>Boolean(x));
  }

  function boardState () {
    return {
      slotKeys: variables.value.map(slot => slot.key),
      script: script.value,
    };
  }

  async function createNode (action, position) {
    console.log("Create Node", action, position);

    let nextId = 0;
    for (let stepId in script.value.nodes) {
      let isInt = isFinite(stepId) && Math.floor(stepId) == Number(stepId);
      if (isInt && stepId >= nextId) {
        nextId = parseInt(stepId) + 1;
      }
    }
    nextId = String(nextId)

    let step = {
      type: action,
      x: position.x, y: position.y,
      inputs: {}, outputs: {},
    };


    let instance = await horde.getInstance('script', script.value.id);
    await instance.update(['nodes', nextId], step);

    return nextId;
  }


  // === Tasks === //

  // TODO: TaskList is not reactive

  const tasksRaw = ref([]);
  const tasks = computed(() =>
    reactive(tasksRaw.value
      .filter(it => it.scriptId == script.value.id)
      .map(it => new Task(it.id))
    )
  );
  var _tasksCancel = null;


  onMounted(async () => {
    //_tasksCancel = await horde.observeIndex('.horde/tasks', newval => tasksRaw.value = newval);
  });

  onUnmounted(() => {
    if (_tasksCancel) {
      _tasksCancel();
      _tasksCancel = null;
    }
  });

  class Task {
    constructor (id) {
      this.id = id;
      this.collapsed = true;
      this.running = false;

      this.loadAll();

      this.variables = {
        lorem: "Ipsum Dolor",
        lugar: "Mancha",
      };

      this.ref = shallowRef(this);
    }

    updateRef () {
      triggerRef(this.ref);
    }

    get title () {
      return "Task #" + this.id;
    }

    toggle () {
      this.collapsed = !this.collapsed;
      this.updateRef();
    }

    navigate () {
      ui.goto(`/task/${this.id}`);
    }

    async run () {
      await horde.handle('task/run', {
        taskId: this.id,
      });
    }

    // Compute task information based on the task object
    computeDetails () {
      if (this._data.currentStep != this.stepId) {
        if (this.stepId != null) {
          nodeActiveCount[this.stepId]--;
        }

        this.stepId = this._data.currentStep;

        if (this.stepId != null) {
          let active = nodeActiveCount[this.stepId];
          nodeActiveCount[this.stepId] = (active || 0) + 1;

          this.running = !this._data.paused;
        } else {
          this.running = false;
        }
      }

      this.variables = Object.assign({}, this._data.variables);
      this.updateRef();
    }

    // Loads all the data from the task object
    async loadAll () {
      let instance =  await horde.getInstance('task', this.id);
      this._data = await instance.get();
      this.computeDetails();
    }

    cssClass () {
      let classes = [];
      if (this.running) {
        classes.push('active');
      }
      return classes;
    }
  }

  async function createTask () {
    ui.alert("Task Creation is not yet implemented");
    return;

    /*
    let res = await horde.handle('model/create', {
      model: 'task',
    });

    console.log('Created Task', res);

    let taskId = res.id;

    await horde.handle('model/update', {
      model: 'task',
      id: taskId,
      subpath: 'scriptId',
      value: script.value.id,
    });

    ui.goto(`/task/${taskId}`)
    */
  }



  // === Board Items === //

  function slotActionClass (slot, property) {
    if (slot[property]) {
      return 'variable-' + property;
    } else {
      return 'variable-disabled';
    }
  }

  async function slotToggle (slot, slotIndex, property) {
    await updateScript(
      ['variables', slotIndex, property],
      slot[property] ? null : true
    );
  }

  async function slotDelete (slotIndex) {
    await updateScript(['variables', slotIndex], null);
  }

  async function slotSchemaEdit (slot, slotIndex) {
    ui.editValue(null, JSON.stringify(slot.schema || {}), async rawval => {
      let newval;
      try {
        newval = JSON.parse(rawval);
      } catch (e) {
        ui.alert("Invalid field schema (invalid JSON)\n" + e.toString());
        return;
      }

      if (!(newval instanceof Object)) {
        ui.alert("Invalid field schema (not an object)\n" + rawval);
        return;
      }

      await updateScript(['variables', slotIndex, 'schema'], newval);
    });
  }

  // === Basic Actions === //

  function editTitle () {
    ui.editValue(null, script.value.title, async newval => {
      updateScript(['name'], newval);
    });
  }

  async function deleteScript () {
    if (window.confirm(`Are you sure you want to delete this script?`)) {
      let instance =  await getScriptInstance();
      await instance.remove();
      ui.goto(`/`)
    }
  }

  function duplicateScript () {
    ui.editValue(null, script.value.name, async newTitle => {
      let instance = await horde.createInstance('script', {
        name: newTitle,
        nodes: script.value.nodes,
        variables: script.value.variables,
      });

      ui.goto(`/script/${instance.id}`)
    });
  }

  function editScriptHandle () {
    ui.editValue(null, script.value.handle, async newval => {
      await updateScript(['handle'], newval);
    });
  }



  // === Sidebar UI === //

  function modeButtonClass (mode) {
    let classes = ["mode-button"];
    if (editMode.value == mode) {
      classes.push("active");
    }

    return classes;
  }

  function modeButtonSelect (mode) {
    console.log('select mode', mode);
    editMode.value = mode;
  }





  // === UI Actions === //

  let mousePos = { x: 0, y: 0 };
  let dragState = null;
  let editMode = ref('select');

  const containerElem = ref(null);
  const sidebarCollapsed = ref(false);
  const sidebarDetailsCollapsed = ref(false);
  const selectBox = reactive({
    visible: false,
    start: {x: 0, y: 0},
    end: {x: 0, y: 0},

    cssStyle () {
      return {
        left: Math.min(selectBox.start.x, selectBox.end.x) * SCALE + 'px',
        top: Math.min(selectBox.start.y, selectBox.end.y) * SCALE + 'px',
        width: Math.abs(selectBox.start.x - selectBox.end.x) * SCALE + 'px',
        height: Math.abs(selectBox.start.y - selectBox.end.y) * SCALE + 'px',
      }
    }
  });

  const draggingEdge = ref(null);

  const slotNameElem = ref(null);

  const allEdges = computed(() => {
    if (dragState && dragState.mode == "vertex") {
      return [dragState.edgeRef.value, ...edges.value];
    } else {
      return edges.value;
    }
  });

  function getMouseBoardPosition (event) {
    let elem = containerElem.value;
    let rect = elem.getBoundingClientRect();

    let rx = event.clientX - rect.left;
    let ry = event.clientY - rect.top;
    let scale = viewScale();

    return { x: rx / scale, y: ry / scale };
  }

  function findHoveredNodes (event) {
    let elem = containerElem.value;

    let cardElements = Array.from(elem.getElementsByClassName('card'));
    let hoveredElements = cardElements.filter(elem => {
      let rect = elem.getBoundingClientRect();

      let h = event.clientX > rect.left && event.clientX < rect.right;
      let v = event.clientY > rect.top && event.clientY < rect.bottom;

      if (h && v) {
        return true;
      }
    });

    let hoveredNodes = hoveredElements.map(elem => {
      let nodeId = elem.getAttribute("x-node-id");
      return nodes.value.find(node => node.id == nodeId);
    });

    return hoveredNodes;
  }

  function boardLeftMouseDown (event, element) {
    if (editMode.value == "move") {
      dragState = {
        mode: 'pan',
      };
    } else if (editMode.value == "zoom") {
      dragState = {
        mode: 'zoom',
        startValue: nav.zoom,
      };
    } else {
      dragState = {
        mode: 'select',
      };
      selectBox.visible = true;
      selectBox.end = selectBox.start = getMouseBoardPosition(event);
    }
  }

  function boardMiddleMouseDown (event, element) {
    dragState = {
      mode: 'pan',
    };
  }

  function windowMouseUp (event) {
    if (dragState) {
      switch (dragState.mode) {
        case 'move': {
          for (let node of dragState.nodes) {
            node.sendUpdates([
              ['x'], ['y']
            ]);
          }
          break;
        }

        case 'resize': {
          dragState.node.sendUpdates([
            ['w'], ['h']
          ]);
          break;
        }

        case 'select': {
          // TODO: Apply selection
          selectBox.visible = false;
          break;
        }

        case 'vertex': {
          // Force allEdges to update by updating edges
          let nodes = findHoveredNodes(event);
          if (nodes.length == 1) {
            dragState.vertex.connect(nodes[0].id);
          } else if (nodes.length > 1) {
            ui.alert("Edge dropped on multiple nodes");
          }

          triggerRef(edges);
          break;
        }
      }

      dragState = null;
    }
  }

  function windowMouseMove (event) {

    let scale = viewScale();
    let delta = {
      x: (event.clientX - mousePos.x) / scale,
      y: (event.clientY - mousePos.y) / scale,
    };

    if (dragState) {
      switch (dragState.mode) {
        case 'move': {
          for (let node of dragState.nodes) {
            node.move(delta.x, delta.y);
          }
          break;
        }

        case 'vertex': {
          // Detect which node is being hovered
          dragState.endNode.x += delta.x;
          dragState.endNode.y += delta.y;
          triggerRef(dragState.edgeRef);
          break;
        }

        case 'resize': {
          // Detect which node is being hovered
          let node = dragState.node;
          let mult = dragState.direction == 'left' ? -2 : 2;
          node.resize(
            delta.x * mult,
            delta.y
          );
          break;
        }

        case 'pan': {
          let z = zoomScale();
          nav.x += delta.x * z;
          nav.y += delta.y * z;
          break;
        }

        case 'zoom': {
          nav.zoom += delta.y;
          break;
        }

        case 'select': {
          selectBox.end = getMouseBoardPosition(event);
          break;
        }
      }
    }

    mousePos.x = event.clientX;
    mousePos.y = event.clientY;
  }

  async function boardContextMenu (event) {
    let clickPos = getMouseBoardPosition(event);
    let actions = await schemas.getActionList();

    let uncategorized = [];
    let categories = {};

    const regex = /^(@?[\w-]+)\//;

    for (let action of actions) {
      let match = action.match(regex);
      if (match) {
        let cat = categories[match[1]] ||= [];
        cat.push(action);
      } else {
        uncategorized.push(action);
      }
    }

    function actionToEntry (action) {
      return {value: 'action:' + action, title: action};
    }

    let addEntries = uncategorized.map(actionToEntry);
    for (let catName in categories) {
      addEntries.push({
        title: catName,
        entries: categories[catName].map(actionToEntry),
      });
    }

    let sel = await ui.contextMenu([
      {title: 'Add Node', icon: 'fa-square-plus', entries: addEntries}
    ]);

    // User cancelled context menu
    if (!sel) return;

    if (sel.startsWith('action:')) {
      createNode(sel.slice('action:'.length), clickPos);
    } else {
      ui.alert("Unsupported board action: " + sel);
    }
  }

  function boardMouseWheel (event) {
    //console.log("mousewheel"); return;
    //if (event.target != boardElem.value) return;

    if (event.deltaY != 0) {
      // positive Y = scroll down = zoom out
      // negative Y = scroll up = zoom in

      let startZoom = zoomScale();

      if (event.deltaY > 0) {
        nav.zoom--;
      } else {
        nav.zoom++;
        if (nav.zoom > 0) {
          nav.zoom = 0;
        }
      }

      let endZoom = zoomScale();
      let scaleDiff = endZoom / startZoom;

      nav.x = Math.floor(nav.x * SCALE * scaleDiff) / SCALE;
      nav.y = Math.floor(nav.y * SCALE * scaleDiff) / SCALE;

      event.stopPropagation();
      event.preventDefault();
    }
  }

  function nodeHeaderMouseDown (node) {
    dragState = {
      mode: 'move',
      nodes: [node]
    };
  }

  function nodeMouseOver (node) {
    dragState = {
      mode: 'node',
      nodes: [node]
    };
  }

  function vertexMouseDown (event, vertex) {
    // Not really a node, but as long as it has x and y fields, Edge will take it
    let endNode = getMouseBoardPosition(event);

    dragState = {
      mode: 'vertex',

      vertex: vertex,
      node: vertex.node,

      endNode: endNode,
      edgeRef: shallowRef(new Edge(vertex, endNode)),
    };

    // Force allEdges to update by updating edges
    triggerRef(edges);
  }

  function nodeCornerMouseDown (node, direction) {
    dragState = {
      mode: 'resize',
      node: node,
      direction,
    };
  }

  async function nodeContextMenu (node) {
    let sel = await ui.contextMenu([
      {value: 'duplicate', icon: 'fa-copy', title: 'Duplicate Node'},
      {value: 'disconnect', icon: 'fa-scissors', title: 'Disconnect Node'},
      {value: 'delete', icon: 'fa-trash', title: 'Delete Node'},
      {value: 'make-start', icon: 'fa-flag', title: 'Make Starting Node'},
      {value: 'inspect', icon: 'fa-eye', title: 'Inspect Node Details'},
    ]);

    if (!sel) return;

    switch (sel) {
      case 'delete': {
        node.delete();
        break;
      }
      case 'make-start': {
        node.makeStart();
        break;
      }
      case 'disconnect': {
        node.disconnectAll();
        break;
      }
      case 'duplicate': {
        node.duplicate();
        break;
      }
      case 'inspect': {
        ui.editValue(null, JSON.stringify(node._step, "\n", " "), newjson => {
          if (newjson != null) {
            try {
              node.replaceAll(JSON.parse(newjson));
            } catch (e) {
              console.error(e);
            }
          }
          
        });
        break;
      }
      default: {
        ui.alert("Unknown selection value: " + sel);
      }
    }
  }

  async function saveSlot () {
    let value = slotNameElem.value.value;
    if (value) {
      slotNameElem.value.value = "";

      // Asynchronous
      updateScript(['variables', 'add'], {key: value});
    }
  }

  onMounted(() => {
    window.addEventListener('mousemove', windowMouseMove);
    window.addEventListener('mouseup', windowMouseUp);
  });
  onUnmounted(() => {
    window.removeEventListener('mousemove', windowMouseMove);
    window.removeEventListener('mouseup', windowMouseUp);
  });


</script>

<template>
  <div style="display: flex; flex-direction: row; height: 100vh; overflow: hidden;">
    <button class="nav-button expand-navbar" v-if="sidebarCollapsed" @click="sidebarCollapsed=false;">
      <i class="fa-solid fa-angles-left"></i>
    </button>

    <main>
      <header>
        {{script.name}}
      </header>
    <div class="board-container">
      <div 
        @mousedown.self.left.prevent="boardLeftMouseDown($event)"
        @mousedown.middle.prevent="boardMiddleMouseDown($event)"
        @contextmenu.self.prevent="boardContextMenu($event)"
        @wheel="boardMouseWheel($event)"
        :style="backgroundCssStyle()"
        class="board">

        <div ref="containerElem" :style="boardCssStyle()">
          <svg style="position: absolute; overflow: visible; width: 1px; height: 1px;">
              <path v-for="edge in allEdges" :d="edge.svgPath()" class="connection"></path>
          </svg>

          <div v-for="node in nodes" :key="node.id" :x-node-id="node.id"
            class="card" :class="node.cssClass()" :style="node.cssStyle()"
            @contextmenu.prevent="nodeContextMenu(node)"
          >

            <div class="resize-corner left" @mousedown.left="nodeCornerMouseDown(node, 'left')"></div>
            <div class="resize-corner right" @mousedown.left="nodeCornerMouseDown(node, 'right')"></div>

            <div class="content" style="display: flex; flex-direction: column; height: 100%;">
              <header @mousedown.left.prevent="nodeHeaderMouseDown(node)">
                <div v-if="node.title" class="card-title">
                  {{node.title}}
                </div>

                <div class="card-action">
                  {{node.index}} {{node.action}}
                </div>

                <!--div style="border-bottom: 1px solid var(--color-border);"></div-->
              </header>

              <div style="display: flex; flex-direction: column; flex-grow: 1;">
                <board-step-field v-for="(schema, key) in node.schema.inputs"
                  type="input"
                  :name="key"
                  :schema="schema"
                  :entry="node._step.inputs[key] || {}"
                  :stepIndex="node.index"
                  :boardState="boardState()"
                />

                <board-step-field v-for="(schema, key) in node.schema.outputs"
                  type="output"
                  :name="key"
                  :schema="schema"
                  :entry="node._step.outputs[key] || {}"
                  :stepIndex="node.index"
                  :boardState="boardState()"
                />
              </div>
            </div>
            <div v-for="vertex in node.vertices" class="vertex"
              @mousedown="vertexMouseDown($event, vertex)"
              :style="vertex.cssStyle()">
              <span class="label" v-if="vertex.label">{{vertex.label}}</span>
            </div>
          </div>

          <div v-if="selectBox.visible" class="select-box" :style="selectBox.cssStyle()">
          </div>
        </div>

      </div>
    </div>
    </main>

    <div class="board-sidebar" v-if="!sidebarCollapsed">
      <div class="nav-button" style="text-align: center;" @click="sidebarCollapsed=true;">
        <i class="fa-solid fa-angles-right"></i>
      </div>

      <div style="display: flex; flex-direction: column;">
        <div style="
          display: flex;
          justify-content: space-evenly;
        ">
          <button :class="modeButtonClass('select')" @click="modeButtonSelect('select')"><i class="fa-solid fa-arrow-pointer"></i></button>
          <button :class="modeButtonClass('move')" @click="modeButtonSelect('move')"><i class="fa-solid fa-up-down-left-right"></i></button>
          <button :class="modeButtonClass('zoom')" @click="modeButtonSelect('zoom')"><i class="fa-solid fa-magnifying-glass"></i></button>
        </div>
        <div style="text-align: center;">
          <a href="#" @click.prevent="sidebarDetailsCollapsed = !sidebarDetailsCollapsed">
            {{sidebarDetailsCollapsed ? 'Show Details' : 'Hide Details'}}
          </a>
        </div>
        <template v-if="!sidebarDetailsCollapsed">
          <div>
            <strong>Handle:</strong>
            <span style="
              border: 1px solid var(--color-border);
              padding: 0.1em 1em;
              font-size: 90%;
              border-radius: var(--border-radius);
            ">
              {{script.handle}}
            </span>
            <i class="fa-solid fa-pencil" style="cursor: pointer;" @click="editScriptHandle()"></i>
          </div>

          <div>
            <strong>Visibility:</strong>
            <select v-model="scriptVisibility">
              <option value="private">Private</option>
              <option value="protected">Protected</option>
            </select>
          </div>

          <div style="
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
          ">
            <button @click="editTitle()">
              Edit Title
            </button>
            <button @click="deleteScript()">
              Delete Script
            </button>
            <button @click="duplicateScript()">
              Duplicate Script
            </button>
          </div>
        </template>
      </div>

      <div>
        <h3>Variables</h3>
        <div class="variables-table-wrapper">
          <table class="variables-table">
            <tr v-for="(slot, slotIndex) in variables">
              <td>
                {{slot.key}}
              </td>
              <td>
                <div style="display: flex; justify-content: space-evenly;">
                  <i class="fa-solid fa-right-to-bracket variable-action"
                    :class="slotActionClass(slot, 'input')"
                    @click="slotToggle(slot, slotIndex, 'input')">
                  </i>
                  <i class="fa-solid fa-right-from-bracket variable-action"
                    :class="slotActionClass(slot, 'output')"
                    @click="slotToggle(slot, slotIndex, 'output')">
                  </i>
                  <i class="fa-solid fa-shapes variable-action"
                    @click="slotSchemaEdit(slot, slotIndex)">
                  </i>
                  <i class="fa-solid fa-trash variable-action" @click="slotDelete(slotIndex)"></i>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <input placeholder="Name" ref="slotNameElem" @keydown.enter="saveSlot" />
              </td>
              <td class="variable-save-button" @click="saveSlot()">
                Create
              </td>
            </tr>
          </table>
        </div>
      </div>

      <div>
        <h3>Steps</h3>
      </div>

      <div>
        <h3>Tasks</h3>

        <div class="task-entry-container">
          <button class="task-create-button" @click="createTask()">
            Create Task
          </button>
        </div>

        <div v-for="task in tasks" class="task-entry-container">
          <div class="task-entry" :class="task.cssClass()">
            <header>
              {{task.title}}
            </header>
            <div class="buttons">
              <button @click="task.run()">
                <i class="fa-solid fa-play"></i>
              </button>
              <button>
                <i class="fa-solid fa-turn-down" style="transform: rotate(-90deg);" @click="task.navigate()"></i>
              </button>
              <button @click="task.toggle()">
                <i :class="['fa-solid', task.ref.collapsed ? 'fa-angles-down' : 'fa-angles-up']"></i>
              </button>
            </div>
            <div v-if="!task.collapsed">
              <div class="step">
                step {{task.ref.stepId}}
              </div>
              <table class="vartable">
                <tr v-for="(value, varname) in task.variables">
                  <td class="varname">
                    {{varname}}
                  </td>
                  <td>
                    <input :value="value">
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped>
  .board-container {
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow: hidden;

    flex-grow: 1;

    margin: 1em;

    border: 1px solid var(--color-border);
    border-radius: var(--border-radius);
  }

  .board {
    width: 1px;
    height: 1px;

    min-height: 100vh;

    user-select: none;
    flex-grow: 1;
    flex-shrink: 1;
    width: 100%;
    overflow: hidden;

    background-image: radial-gradient(circle at center, #fff2 0%, transparent 20%);
    background-size: 20px 20px; /* Adjust the size of the gradient dots */
  }

  .board > div {
    border: 1px solid white;

    position: relative;
    left: 50%;
    width: 0;
    height: 0;
  }

  .select-box {
    position: absolute;
    border: 1px dashed #8df8;
    background-color: #8df2;
  }

  main {
    width: 100%;
    height: 100%;

    display: flex;
    flex-direction: column;
  }

  main > header {
    text-align: center;
    border-bottom: 1px solid var(--color-border);
    height: 2.5em;

    font-size: 130%;

    display: flex;
    flex-direction: row;

    justify-content: space-evenly;
    align-items: center;
  }


  /* == Sidebar == */

  .board-sidebar {
    overflow-y: auto;
    width: 20em;
    flex-grow: 0;
    flex-shrink: 0;

    height: 100vh;

    border-left: 1px solid var(--color-border);
  }

  .board-sidebar header {
    display: flex;
    flex-direction: row;
  }

  .nav-button {
    display: block;
    border: none;
    border-radius: 0;
    background-color: transparent;

    width: 100%;

    padding: 0.2em;

    vertical-align: middle;
  }

  .nav-button:hover {
    background-color: var(--color-green-dark);
  }

  .expand-navbar {
    background-color: var(--color-background);
    border: 1px solid var(--color-border);
    position: absolute;
    top: 0;
    right: 0;
    width: 3em;
    height: 3em;
  }

  .mode-button { width: 2em; }
  .mode-button.active { border-color: var(--color-green); }




  /* == Cards == */

  .card {
    cursor: default;

    position: absolute;
    display: flex;
    flex-direction: column;
    text-align: left;
    padding: 0.4em;
    box-sizing: border-box;

    border: 1px solid #4d4d4f;
    border-radius: 4px;
    white-space: nowrap;
    text-align: center;

    background-color: var(--color-background-soft);
    box-shadow: 1px 2px 5px 1px #0003;


    transform: translate(-50%, 0);
  }

  .card > .content {
    overflow: hidden;
  }

  .card header {
    border-bottom: 1px solid #fff2;
    cursor: move;
  }

  .card-title {
    font-weight: bold;
  }

  .card-action {
    font-family: monospace;
    font-size: 80%;
  }

  /* Card special classes */
  .card.error { border-color: red; }
  .card.start { border-color: aqua; }
  .card.active {
    border-color: lightgreen;
    box-shadow: 0px 0px 8px 1px #fff8;
  }



  /* == Vertices & Connections == */

  .vertex {
    position: absolute;

    border-radius: 100%;
    width: 8px;
    height: 8px;

    border: 1px solid black;
    background-color: white;
    overflow: visible;

    transform: translate(-50%, -50%);
  }

  .vertex .label {
    padding: 0em 0.4em;
    border: 1px solid #fff4;
    background-color: #0008;

    border-radius: var(--border-radius);

    position: absolute;
    transform: translate(-50%, 0.5em);
  }

  .vertex .label:hover {
    background-color: #3338;
  }

  .resize-corner {
    position: absolute;
    width: 14px;
    height: 14px;
    bottom: -7px;
  }
  .resize-corner.right {
    right: -7px;
    cursor: se-resize;
  }
  .resize-corner.left {
    left: -7px;
    cursor: sw-resize;
  }

  path.connection {
    stroke: #fff4;
    stroke-width: 2px;
    fill: none;
  }



  /* == Variables == */

  .variables-table-wrapper {
    padding: 0.3em;
  }

  .variables-table {
    width: 100%;

    border: 1px solid var(--color-border);
    border-radius: var(--border-radius);

    border-spacing: 0;
    overflow: hidden;
  }

  .variables-table tr {
    border-top: 1px solid var(--color-border);
  }
  .variables-table tr:first-child {
    border-top: none;
  }

  .variables-table td {
    padding: 0.1em 0.3em;
    border-left: 1px solid var(--color-border);
  }
  .variables-table td:first-child {
    border-left: none;
  }

  .variables-table input {
    width: 100%;
    background: none;
    border: none;
    color: inherit;
  }

  .variable-save-button:hover {
    background-color: var(--color-green-dark);
    cursor: pointer;
  }

  .variable-action { cursor: pointer; }
  .variable-input { color: green; }
  .variable-output { color: orange; }
  .variable-disabled { color: dimgray; }



  /* == Tasks == */

  .task-entry-container {
    padding: 0.5em 1em;
  }
  .task-entry {
    border: 1px solid var(--color-border);
    border-radius: var(--border-radius);

    padding: 0.4em 0.7em;
  }
  .task-entry.active {
    border: 1px solid lightgreen;
  }

  .task-entry header {
    font-weight: bold;
    padding-left: 0.3em;
  }

  .task-entry .buttons {
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
  }
  .task-entry button {
    width: 2em;
    height: 2em;
    text-align: center;
  }
  .task-entry .step {
    font-family: monospace;
    font-size: 80%;

    text-align: center;
  }
  .task-entry .varname {
    font-weight: bold;
    padding-right: 0.5em;
  }
  .task-entry input {
    width: 100%;
  }

  .task-entry .vartable {
    width: 100%;
  }

  .task-create-button {
    width: 100%;
  }

</style>
