
import * as horde from './horde';

// File upload chunk size (in character count, plain or base64 encoded)
const CHUNK_SIZE = 200_000;

function fromPath (path) {
  if (path.endsWith('/')) {
    return new Directory(path);
  } else {
    return new File(path);
  }
}

export class File {
  constructor (path) {
    this.path = path;
    this.name = path.split("/").pop();
  }
}

export class Directory {
  constructor (path) {
    this.path = path;

    if (path == "/") {
      this.name = "/";
    } else {
      this.name = path.split("/").slice(-2)[0];
    }

    this.opened = false;
    this.entries = null;
  }

  open () {
    if (this.opened) return;
    this.opened = true;

    if (this.entries == null) {
      horde.handle('files/read', {path: this.path}).then(res => {
        if (res.directory) {
          let entries = res.content.map(name => fromPath(this.path + name));
          entries.sort((a, b) => {
            let adir = a instanceof Directory;
            let bdir = b instanceof Directory;

            // One is a directory and the other one isn't.
            // Directories go before files
            if (adir && !bdir) return -1;
            if (!adir && bdir) return 1;

            // Same entry type. Compare their names
            return a.name.localeCompare(b.name);
          });

          this.entries = entries;
        } else {
          console.error("Not a directory: " + this.path);
        }
      });
    }
  }

  close () {
    this.opened = false;
  }

  toggle () {
    if (!this.opened) {
      this.open();
    } else {
      this.close();
    }
  }

  async uploadBinary (filename, content) {
    if (content.length < CHUNK_SIZE) {
      await horde.handle('files/write', {
        path: this.path + filename,
        binary: true,
        content,
      });
    } else {
      // https://stackoverflow.com/questions/7033639
      let chunks = content.match(new RegExp(`.{1,${CHUNK_SIZE}}`, "g"));

      let stream = await horde.handle('stream/create');

      let writeProm = horde.handle('files/write', {
        path: this.path + filename,
        binary: true,
        content: stream,
      });

      for (let chunk of chunks) {
        await horde.handle('stream/feed', {
          id: stream.id,
          data: chunk,
        });
      }

      await horde.handle('stream/close', { id: stream.id });

      // Await for full file write
      await writeProm;
    }
  }
}