
var configPromise = null;

const CONFIG_PATH = "/config.json";

async function downloadHostConfig () {
    let response = await fetch(CONFIG_PATH);

    if (!response.ok) {
        if (response.status == 404) {
            throw new Error(`No configuration available in: ${CONFIG_PATH}`);
        } else {
            throw new Error(`Request to ${CONFIG_PATH} failed with ${response.statusText} (${response.status})`);
        }
    }

    let data = await response.json();
    return data;
}

async function getHostConfig () {
    if (!configPromise) {
        configPromise = new Promise(async resolve => {
            try {
                let config = await downloadHostConfig();
                resolve(config);
            } catch (e) {
                console.error("Error Downloading Configuration", e);
            }
        });

        configPromise.then(r => console.log("Promise result", r));
    }

    return await configPromise;
}

export async function getUserConfig () {
    return JSON.parse(localStorage.getItem('userConfig') || '{}');
}

export async function storeUserData (newData) {
    let oldConfig = await getUserConfig();
    let newConfig = Object.assign({}, oldConfig, newData);

    localStorage.setItem('userConfig', JSON.stringify(newConfig));
}

export async function getConfig () {
    return Object.assign({}, await getHostConfig(), await getUserConfig());
}

export default getConfig;