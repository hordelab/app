import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

function modelShow (name, mod) {
  return {
    name,
    path: `/${name}/:id`,
    props: true,
    component: mod
  }
}

const router = createRouter({
  //history: createWebHistory(import.meta.env.BASE_URL),
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },

    {
      path: '/signup',
      name: 'signup',
      component: () => import('../views/Signup.vue')
    },

    modelShow('chat', () => import('../views/Chat.vue')),
    modelShow('task', () => import('../views/Task.vue')),
    modelShow('script', () => import('../views/Script.vue')),

    {
        path: `/agent/:id(.*)`,
        name: "agent",
        props: true,
        component: () => import('../views/Agent.vue')
    },

    {
      path: '/files',
      name: 'files',
      component: () => import('../views/Files.vue')
    },

    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('../views/TaskList.vue')
    },

    {
      path: '/marketplace',
      name: 'marketplace',
      component: () => import('../views/Marketplace.vue')
    },
  ]
})



// Convert all links in the document that point to an absolute path into hash route paths

// Function to convert absolute path to hash path
function convertToHashPath(link) {
    const absolutePath = link.getAttribute('href');
    if (absolutePath.startsWith('/')) {
        const hashPath = '#' + absolutePath;
        link.setAttribute('href', hashPath);
    }
}

// Initial conversion for existing links
const initialLinks = document.querySelectorAll('a[href^="/"]');
initialLinks.forEach(link => convertToHashPath(link));

// Mutation Observer to handle new nodes and attribute changes
const observer = new MutationObserver(function(mutations) {
    mutations.forEach(mutation => {
        // Handle added nodes
        if (mutation.type === 'childList') {
            mutation.addedNodes.forEach(node => {
                if (node.tagName === 'A') {
                    convertToHashPath(node);
                } else if (node.querySelectorAll) {
                    // Handle links in nested elements
                    const nestedLinks = node.querySelectorAll('a[href^="/"]');
                    nestedLinks.forEach(link => convertToHashPath(link));
                }
            });
        }
        // Handle attribute changes
        if (mutation.type === 'attributes' && mutation.attributeName === 'href') {
            const node = mutation.target;
            if (node.tagName === 'A') {
                convertToHashPath(node);
            }
        }
    });
});

// Configure the observer to watch for childList changes and attribute changes
observer.observe(document.body, { childList: true, subtree: true, attributes: true, attributeFilter: ['href'] });


export default router
