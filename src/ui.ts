
export function goto (path) {
    window.location.hash = "#" + path;
}

export async function confirm (prompt) {
    return window.confirm(prompt);
}

export async function input (prompt) {
    return window.prompt(prompt);
}

export async function inputFile (options) {
    console.log("app.inputFile", options);
    throw new Error("Cannot open files");
}

export function alert (message) {
    emit('alert', {message});
}

export function editValue (mode, value, callback) {
    emit('edit-value', {mode, value, callback});
}

export function contextMenu (entries) {
    return new Promise(resolve => {
        emit('context-menu', {entries, callback: resolve});
    });
}


// === Events === //

const listeners = {};

export function emit (name, ...args) {
    const fs = listeners[name];
    if (fs == null) return;

    for (let f of fs) {
        f(...args);
    }
}

export function listen (name, fn) {
    let fs = listeners[name];
    if (fs == null) {
        listeners[name] = fs = [];
    }

    fs.push(fn);
}

export function invoke (name, ...args) {
    throw new Error("invoke");
}

window.addEventListener("unhandledrejection", function(promiseRejectionEvent) { 
    console.error(promiseRejectionEvent.reason);
    alert("Unhandled Error: " + String(promiseRejectionEvent.reason));
});