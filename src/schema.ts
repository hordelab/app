
import * as horde from './horde';

// Actions that might not be listed by the system due to their special nature
const defaultActions = {
    'signal/receive': {
        description: "Starts executing a new path when the signal is received",
        inputs: {'signal': {type: 'string'}}
    }
};

export class SchemaReader {
    constructor () {
        //this.horde = horde;
        this.actionsPromise = null;
    }

    async actionSchemas () {
        if (!this.actionsPromise) {
            //const horde = this.horde;

            this.actionsPromise = new Promise(async resolve => {
                let res = await horde.handle('system/actions');
                let allActions = Object.assign({}, defaultActions, res.actions);
                resolve(allActions);
            });
        }

        return this.actionsPromise;
    }

    async getAction (actionName) {
        let actions = await this.actionSchemas();
        return actions[actionName];
    }

    async getActionList () {
        let actions = await this.actionSchemas();
        return Object.keys(actions);
    }
}

// Represents and controls a field instance, and connects the UI input
// for the field with the instance in the horde instance.
export class Field {
    constructor ({value, schema, callback}) {
        this.value = value;
        this.schema = schema || {};
        this.callback = callback;

        this.computeDisplay();
    }

    emit (value, subpath) {
        if (!this.callback) return;

        if (value === undefined) {
            value = this.value;
        }

        subpath = subpath || [];
        this.callback(value, subpath);
    }

    computeDisplay () {
        const schema = this.schema;

        // Simple Schemas

        if (schema.enum) {
            this.displayMode = 'enum';
            this.options = schema.enum.map(val => ({
              value: val,
              title: val,
            }));
            return;
        }

        // Complex Schemas

        let validModes = new Set();
        let anyMode = false;

        function addType (type) {
          if (type instanceof Array) {
            type.forEach(addType);
          } else if (type == "string") {
            validModes.add('string');
          } else if (type == "null") {
            validModes.add('null');
          } else if (type == "boolean") {
            validModes.add('boolean');
          } else {
            anyMode = true;
          }
        }

        function addSubschema (subschema) {
          if (subschema.type) addType(subschema.type);
          else {
            anyMode = true;
          }
        }

        if (schema.type) {
          addType(schema.type);
        } else if (schema.oneOf instanceof Array) {
          schema.oneOf.forEach(addSubschema);
        } else {
          anyMode = true;
        }

        if (typeof this.value == 'string' && validModes.has('string')) {
            this.displayMode = 'string';
        }

        if (this.value == null && validModes.has('null')) {
            this.displayMode = 'null';
        }

        // If only one mode is allowed and it wasn't known from the value
        if (!this.displayMode && !anyMode && validModes.size == 1) {
          // Extract the mode from the set
          this.displayMode = Array.from(validModes)[0];
        }
    }
}